// mini-activity

/*function printPalayaw(){
	let nickname = prompt("What is your nick name?");
	console.log("Hello, " + nickname);
};

printPalayaw();
*/
/*
	Functions are lines/blocks of codes that tell our devices/application to perform certain task/k when called/ivoked.
	Functions are mostly created to create complicated tasks to run several lines of codes in succession,
	They are also used to prevent repeating lines/blocks of codes that perform the same task
*/

// However, for some use cases, this may not be ideal. For other cases, functions can also process data directly passed into it instead of relying only on global variables such as prompt()

// Consider this function
// we can directly pass data into the function. as for the function below,the data is referred to as the "name" within the printName function. this "name" is what we called as the parameter.
/*
	PARAMETER
		-it acts as a named variable/container that exists only inside of a function;
		it is used to store information that is provided to a function when it is called/invoked;
*/
function printName(name){
	console.log("Hello " + name);
}
// "Joana", the information provided into the function, is called an argument. Values passed when invoking a function are called arguments. These arguments are then stored inside as the parameters within the function.
printName("Joana");
// These two are both arguments since both of them are supplied as information that will be used to print out the full message of function. when the printName() function is called, "Toppie" and "Sydney" are stored in the parameter "name" then the function uses them to print the message
printName("Toppie");
printName("Sydney");

function printNum(num1, num2){
	console.log("the numbers passed as arguments are: ");
	console.log(num1);
	console.log(num2);
};

printNum(10,22);


function printFriends(friend1, friend2, friend3){
	console.log("My Friends are " + friend1 +" "+ friend2 +" "+ friend3);
}

printFriends("Ryle", "Dexter", "Kiko");

function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " is dibisible by 8?")
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
// We can also do the same using prompt(). However, take not that using prompt() outputs a string data type. Strings data types are not ideal and cannot be used for mathematical computations
checkDivisibilityBy8(63);

function checkDivisibilityBy4(num){
	let remainder = num%4;
	console.log("The remainder of " + num + " is " + remainder);
	let isDivisibleBy4 = remainder === 0;
	console.log("Is " + num + " is dibisible by 4?")
	console.log(isDivisibleBy4);
}
checkDivisibilityBy4(32654684);
checkDivisibilityBy4(3260156157);

/*function isEven(number1){
	console.log(number1 % 2 === 0)
	let numEven = isEven(20);
	console.log(numEven);
}

function isOdd(number2){
	let number2 !== 0;
	let numOdd = isOdd(31);
	console.log(numOdd);
}

isEven(68);
isOdd(67);


*/


/*
FUNCTIONS AS ARGUMENTS
	function parameters can also accept other functions as arguments
	some complex functions use other functions as arguments to perform complicated tasks/resulet
		This will be further discussed when we tackle array methods
*/


function argumentFunction(){
	console.log("This function is passed into another function.");
};

function invokeFunction(functionParameter){
	functionParameter();
}
// adding and removing of the parenthesis impacts the output of JS heavily. when a function is used with parenthesis, it denotes that invoking/calling a function. - that is why when we call the function, the functionParameter as an argument does not have any parenthesis.
// if the function as parameter does not have parenthesis, the function as an argument should have parenthesis to denote that it is a function being passed as an argument
invokeFunction(argumentFunction);

/*function printFullName(firstName, middleName, lastName){
	console.log(firstName + '' + middleName + '' + lastName);
}
printFullName('Juan', 'Dela', 'Cruz');*/

function returnFullName(firstName, middleName, lastName){
	return firstName + " " + middleName + " " + lastName;
	//console.log("This message will not be printed");
}

console.log(returnFullName("Jeffrey", "Smith", "Bezos"));

let completeName = returnFullName("Jeffrey", "Smith", "Bezos");
console.log(completeName);

function returnAddress(barangay, city){
	return barangay + " " + city;
}
console.log(returnAddress("Bambang", "Pasig City"));

function printPlayerInfo(username,level,job){
	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);
}

let user1 = printPlayerInfo("Fingertastic", 99, "Assassin");
console.log(user1);
