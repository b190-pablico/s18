/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// 1.
function printSum(number1, number2){
	console.log("Displayed sum of " + number1 + " and " + number2);
	console.log(number1 + number2);
}
printSum(22, 23);

function printDifference(number3, number4){
	console.log("Displayed difference of " + number3 + " and " + number4);
	console.log(number3 - number4);
}
printDifference(150, 35);

// 2
function returnProduct(number5, number6){
	console.log("The product of " + number5 + " and " + number6 +":");
	return number5 * number6;
}
let product = returnProduct(5, 5);
console.log(product);

function returnQuotient(number7, number8){
	console.log("The quotient of " + number7 + " and " + number8 +":");
	return number7 / number8;
}
let quotient = returnQuotient(25, 5);
console.log(quotient);

// 3
function areaCircle(pie, radius){
	console.log("The result of getting the area of a circle with " + radius + " radius");
	return pie * (radius ** 2);
}
let circleArea = areaCircle(3.141, 15);
console.log(circleArea);

// 4
function average(number9, number10, number11, number12){
	console.log("The average of " + number9 + ", " + number10 + ", " + number11 + " and " + number12 + ":");
	return (number9 + number10 + number11 + number12) / 4;
}
let averageVar = average(100, 90, 80, 75);
console.log(averageVar);

// 5
function testScores(number13, number14){
	console.log("Is " + number13 + "/" + number14 + " a passing score?")
	return (number13/number14)*100;
}
let isPassing = testScores(45,75);
let isPassingScore = isPassing>100

